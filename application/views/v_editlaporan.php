<!doctype html>
<html lang="en">
<head>
  <style type="text/css">
    .form-control{
      width: 50%;
    }
  </style>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">

  <title>List Laporan, Accuracy Triage Natural Disaster</title>
</head>
<body>
  <?php $this->load->view('menu');?> <!--Include menu-->

  <div class="container-fluid">
    <center><h1>Edit Laporan</h1></center>
    <?php foreach ($laporan as $l) {?>
      <form action="<?php echo base_url(). 'crud/update'; ?>" method="post">
          <br/>
        <div class="form-group">
          <input type="text" class="form-control form-nama" name="nama" readonly value="<?php echo $l->nama ?>" placeholder="Nama Lengkap" required autofocus>
        </div>
    <div>
      <input type="text" class="form-control form-nama" name="nik" readonly value="<?php echo $l->nik ?>"placeholder="NIK" required autofocus>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <br/>
        <input type="text" class="form-control form-email" name="email" readonly value="<?php echo $l->email ?>" placeholder="Email" required autofocus>
      </div>

    <div class="form-group col-md-6">
      <br>
      <input type="text" class="form-control form-noHp" name="noHp" readonly value="<?php echo $l->noHp ?>" placeholder="Nomor Hp" required autofocus>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-5">
      <input type="text" class="form-control form-alamat" name="alamat" value="<?php echo $l->alamat ?>"placeholder="Alamat Bencana" required autofocus>
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control form-kota" name="kota" value="<?php echo $l->kota ?>"placeholder="Kota" required autofocus>
    </div>  
    <div class="form-group col-md-2">
      <input type="text" class="form-control form-kodepos" name="kodePos" value="<?php echo $l->kodePos ?>" placeholder="Kode Pos" required autofocus>    
    </div>
  </div>  
    <div class="form-group">
      <input type="text" class="form-control form-nama" name="deskripsi" value="<?php echo $l->deskripsi ?>" placeholder="Jenis Bencana" required autofocus>
    </div>
    <div class="form-group">
      <select class="form-control form-nama" name="validasi">
        <option value="Belum Validasi">Belum Validasi</option>
        <option value="Sudah Divalidasi">Sudah Valid</option>
      </select>
    </div>
    <center><button class="btn btn-simpan btn-lg btn-primary btn-block" type="submit">Simpan</button></center>
    </form>
 <style>
  .footer{
    background-color: #blue;
  }
</style>

<style>
  .keterangan{
    padding: 8%;
  }
  .btn-simpan{
    margin: auto;
    width: 190px;
  }
  .form-signin{
  background:rgba(0,0,0,0.3);
  box-shadow: 0px 0px 20px 6px;
  padding-top:1%;
  padding-bottom:1%;
  padding-left: 5%;
  padding-right: 5%;
  margin: 12%;
  background-color: #E2F5F8;
  }
  .form-email{
   margin-right: 15px;
    margin-left: 293px;
    width: 63%;
  }
  .form-nama{
    margin: auto;
    width: 56%;
  }
  .form-noHp{
    margin-right: 0px;
    width:317px;
    margin-left: 50px
  }
  .form-alamat{
    margin-left: 293px;
    margin-right: 0px;
    width: 50%;
  }
  .form-kota{
    margin-right: 0px;
    margin-left: 21px;
    width:230px;
  }
  .cfluid{
  background-color: #B1CAF8;
  }
  .form-kodepos{
    margin-right: 222px;
    margin-left: 41px;
  }
</style>
<?php  }?>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/propper.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
</body>
</html>
