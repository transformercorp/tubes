<!-- Collect the nav links, forms, and other content for toggling -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
  <!--Akses Menu Untuk Admin-->
  <?php if($this->session->userdata('akses')=='1'):?>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Page'?>">Home </span></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Crud/lihatlaporan'?>">Laporan </span></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Identifikasi/triase'?>">Triase </span></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'List_user'?>">List User</span></a></li>
      </span></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'List_tim'?>">Kirim Tim Medis</span></a></li>
  <!--Akses Menu Untuk Dosen-->
  <?php elseif($this->session->userdata('akses')=='2'):?>
     <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Page'?>">Home </span></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Page/identifikasi'?>">Identifikasi </span></a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Identifikasi/triase'?>">Triase </span></a></li>
  <!--Akses Menu Untuk User-->
  <?php elseif($this->session->userdata('akses')=='3'):?>

      <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Page'?>">Home </span></a></li>
     <!--<li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Page/berita'?>">Berita </span></a></li> -->
            <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Crud'?>">Laporan </span></a></li>
  <?php else:?>

      <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Welcome'?>">Home </span></a></li>
    <!--  <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Welcome/berita'?>">Berita </span></a></li>-->
      <!--<li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Welcome/tentang'?>">Tentang </span></a></li>-->
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url().'Login'?>">Laporan </span></a></li>
  <?php endif;?>
  </ul>
<?php if($this->session->userdata('akses')=='1'||$this->session->userdata('akses')=='2'||$this->session->userdata('akses')=='3'):?>
  <form class="nav-item"><a class="nav-link" href="<?php echo base_url().'Welcome'?>"><?php echo $this->session->userdata('ses_nama');?> </span></a>
    </form>
    <form class="form-inline my-2 my-lg-0" action="<?php echo base_url().'Login/logout'?>">
     <button type="logout" class="btn btn-outline-primary">Sign Out</button>
</form>
<?php else:?>
  <form class="nav-item"><a class="nav-link" href="<?php echo base_url().'Login'?>">Sign In</span></a>
  </form>
    <form class="form-inline my-2 my-lg-0" action="<?php echo base_url().'Signup'?>">
     <button type="logout" class="btn btn-outline-primary">Sign Up</button>
    </form>
  <?php endif;?>
  </div>
</nav>
