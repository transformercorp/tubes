<!doctype html>
<html lang="en">
<head>
  <style type="text/css">
    .form-control{
      width: 50%;
    }
  </style>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">

  <title>List Tim, Accuracy Triage Natural Disaster</title>
</head>
<body>
  <?php $this->load->view('menu');?> <!--Include menu-->

  <div class="container-fluid">
  <center><h1>List Tim Medis</h1></center>
  <?php foreach ($user as $t) {?>
   <form action="<?php echo base_url(). 'List_user/update'; ?>" method="post">
          <br/>
    <legend><center>Edit Tim Medis</center></legend>
    <br/>
    <div class="form-group">
      <input type="text" class="form-control" name="nama"  value="<?php echo $t->nama ?>" placeholder="Nama Lengkap" required autofocus>
    </div>
    <div>
      <input type="text" class="form-control" name="nik"  value="<?php echo $t->nik ?>"placeholder="NIK" required autofocus>
    </div>
    <div> 
      <br/>
      <input type="text" class="form-control" name="email"  value="<?php echo $t->email ?>" placeholder="Email" required autofocus>
    </div>
    <div>
      <br>
      <input type="text" class="form-control" name="noHp"  value="<?php echo $t->noHp ?>"  placeholder="Nomor Hp" required autofocus>
    </div>
     <br/>
     <br/>
     <br/>
     <br/>
     <br/>
  <center>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Edit</button>
  </center>
</form>
</div>
<?php  }?>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/propper.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
</body>
</html>