<?php 


class List_tim extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_list_tim');
		$this->load->helper('url');

	}

	function index(){
	$data['tim'] = $this->m_list_tim->tampil_data()->result();
	if($this->session->userdata('akses')=='1'){
      $this->load->view('list_tim',$data);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
  		}
	}
	function hapus($nik){
		$where = array('nik' => $nik);
		$this->m_list_tim->hapus_data($where,'tim');
		redirect('list_tim');
	}
	function edit($nik){
		$where = array('nik' => $nik);
		$data['tim'] = $this->m_list_tim->edit_data($where,'tim')->result();
		$this->load->view('v_edittim',$data);
	}


	function kirim($nik){
		$where = array('nik' => $nik);
		$data['tim'] = $this->m_list_tim->kirim_medis($where,'tim')->result();
		$this->load->view('kirim_medis',$data);
	}

	function update(){
		$nama = $this->input->post('nama');
		$nik = $this->input->post('nik');
		$email = $this->input->post('email');
		$noHp = $this->input->post('noHp');
		$status = $this->input->post('status');

	$data = array(
		'nama' => $nama,
		'nik' => $nik,
		'email' => $email,
		'noHp' => $noHp,
		'status'=>$status
	);

	$where = array(
		'nik' => $nik
	);

	$this->m_list_tim->update_data($where,$data,'tim');
	redirect('list_tim');
}
}