<?php 


class List_user extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_list_user');
		$this->load->helper('url');

	}

	function index(){
	$data['user'] = $this->m_list_user->tampil_data()->result();
	if($this->session->userdata('akses')=='1'){
      $this->load->view('list_user',$data);
    }else{
      echo "Anda tidak berhak mengakses halaman ini";
  		}
	}
	function hapus($nik){
		$where = array('nik' => $nik);
		$this->m_list_user->hapus_data($where,'user');
		redirect('List_user');
	}
	function edit($nik){
		$where = array('nik' => $nik);
		$data['user'] =$this->m_list_user->edit_data($where,'user')->result();
		$this->load->view('v_edituser',$data);
	}
	function update(){
		$nama = $this->input->post('nama');
		$nik = $this->input->post('nik');
		$email = $this->input->post('email');
		$noHp = $this->input->post('noHp');

	$data = array(
		'nama' => $nama,
		'nik' => $nik,
		'email' => $email,
		'noHp' => $noHp
	);

	$where = array(
		'nik' => $nik
	);

	$this->m_list_user->update_data($where,$data,'user');
	redirect('list_user');
}
}